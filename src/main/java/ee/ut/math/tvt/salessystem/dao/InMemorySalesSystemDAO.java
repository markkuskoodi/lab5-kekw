package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldShoppingCart;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.ArrayList;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<SoldShoppingCart> soldShoppingCartList;
    private long soldIdCount;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.soldShoppingCartList = new ArrayList<>();
        this.soldIdCount = 0;
    }


    public long getSoldIdCount() {
        return soldIdCount;
    }

    public void setSoldIdCount(long soldIdCount) {
        this.soldIdCount = soldIdCount;
    }


    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    public SoldShoppingCart findSoldShoppingCart(long id) {
        for (SoldShoppingCart cart : soldShoppingCartList) {
            if (cart.getId() == id) {
                return cart;
            }
        }
        return null;
    }

    @Override
    public void close() {

    }

    @Override
    public List<SoldShoppingCart> getAllSoldShoppingCarts() {
        return soldShoppingCartList;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    public void saveSoldShoppingCart(SoldShoppingCart cart) {
        soldShoppingCartList.add(cart);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        if (stockItem.getQuantity() < 0) {
            throw new SalesSystemException();
        }
        stockItemList.add(stockItem);
    }

    public List<SoldShoppingCart> getSoldShoppingCartList() {
        return soldShoppingCartList;
    }

    @Override
    public void beginTransaction() {
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
    }
}
