package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldShoppingCart;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShoppingCart {

    private static final Logger log = LogManager.getLogger(ShoppingCart.class);
    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();


    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {
        // TODO In case such stockItem already exists increase the quantity of the existing stock
        if (item.getQuantity() < 0) {
            throw new SalesSystemException("Quantity can't be negative");
        }
        int warehouseQuantity = dao.findStockItem(item.getId()).getQuantity();
        boolean wasnt = true;

        for (SoldItem cartItem : items) {
            if (cartItem.getId() == item.getId()) {
                int newQuantity = cartItem.getQuantity() + item.getQuantity();
                if (newQuantity > warehouseQuantity) {
                    throw new SalesSystemException("The quantity of selected product is only " + warehouseQuantity + " in the warehouse");
                } else {
                    cartItem.setQuantity(newQuantity);
                    log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
                }
                wasnt = false;
            }
        }

        while (wasnt) {
            if (item.getQuantity() > warehouseQuantity) {
                throw new SalesSystemException("The quantity of selected product is only " + warehouseQuantity + " in the warehouse");
            } else {
                items.add(item);
                log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
            }
            wasnt = false;
        }
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        items.clear();
        log.debug("Shopping cart emptied");
    }

    public void submitCurrentPurchase() {
        // TODO decrease quantities of the warehouse stock
        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        dao.beginTransaction();
        log.info("Begin transaction");
        try {
            for (SoldItem item : items) {
                dao.saveSoldItem(item);
                StockItem n = dao.findStockItem(item.getStockItem().getId());
                n.setQuantity(n.getQuantity() - item.getQuantity());
            }
            List<SoldItem> tooted = new ArrayList<>();
            for (SoldItem item : items) {
                tooted.add(item);
            }
            SoldShoppingCart cart = new SoldShoppingCart(dao, tooted);
            dao.saveSoldShoppingCart(cart);
            dao.commitTransaction();
            items.clear();
            log.info("Transaction done");
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

}
