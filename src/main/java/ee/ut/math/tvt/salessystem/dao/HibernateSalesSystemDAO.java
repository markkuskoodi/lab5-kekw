package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldShoppingCart;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    // TODO implement missing methods

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public List<StockItem> findStockItems() {
        return em.createQuery("from StockItem", StockItem.class).getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        try {
            StockItem item = em.find(StockItem.class, id);
            return item;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    @Override
    public SoldShoppingCart findSoldShoppingCart(long id) {
        try {
            SoldShoppingCart item = em.find(SoldShoppingCart.class, id);
            return item;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        //em.getTransaction().begin();
        em.merge(stockItem);
        em.flush();
        //em.getTransaction().commit();
    }


    @Override
    public void saveSoldItem(SoldItem item) {
        //em.getTransaction().begin();
        //if (!em.contains(item)) {
            em.merge(item);
            em.flush();
        //}
        //em.getTransaction().commit();
    }

    @Override
    public void saveSoldShoppingCart(SoldShoppingCart cart) {
        em.merge(cart);
        em.flush();
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }





    @Override
    public List<SoldShoppingCart> getAllSoldShoppingCarts() {
        return em.createQuery("from SoldShoppingCart", SoldShoppingCart.class).getResultList();
    }

    @Override
    public long getSoldIdCount() {
        return 0;
    }

    @Override
    public void setSoldIdCount(long idCount) {

    }
}