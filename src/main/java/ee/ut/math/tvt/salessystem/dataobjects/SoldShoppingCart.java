package ee.ut.math.tvt.salessystem.dataobjects;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

public class SoldShoppingCart {
    private final SalesSystemDAO dao;

    private LocalDate date;
    private String time;
    private Timestamp timestamp;
    private List<SoldItem> items;
    private long id;
    private double sum;

    public SoldShoppingCart(SalesSystemDAO dao, List<SoldItem> cart) {
        this.dao = dao;
        this.date = LocalDate.now();
        this.time = (new Timestamp(System.currentTimeMillis())).toString().split(" ")[1];
        this.timestamp = new Timestamp(System.currentTimeMillis());
        items = cart;
        id = dao.getSoldIdCount() + 1;
        dao.setSoldIdCount(id);
        this.sum = 0;
        for (SoldItem item: items) {
            sum += item.getPrice() * item.getQuantity();
        }
    }

    public LocalDate getDate() {return date;}

    public String getTime() {return time;}

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public double getSum() {
        return sum;
    }

    public long getId() {
        return id;
    }
}
