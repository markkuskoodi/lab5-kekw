package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;


public class Warehouse {

    private static final Logger log = LogManager.getLogger(Warehouse.class);
    private final SalesSystemDAO dao;
    long idCount;

    public Warehouse(SalesSystemDAO dao) {
        this.dao = dao;
        List<StockItem> stock = dao.findStockItems();
        if (stock.size()==0) {
            idCount = 0;
        } else {
            idCount = stock.get(stock.size() - 1).getId();
        }
    }

    public void addProduct(StockItem stockitem) {
        dao.beginTransaction();
        if (stockitem.getPrice() < 0) {
            String message = "Price can't be negative";
            dao.rollbackTransaction();
            log.debug(message);
            throw new SalesSystemException(message);
        } else if (stockitem.getQuantity() < 0) {
            String message = "Amount can't be negative";
            log.debug(message);
            dao.rollbackTransaction();
            throw new SalesSystemException(message);
        } else if (stockitem.getId() == 0) {
            idCount++;
            long barcode = idCount;
            stockitem.setId(barcode);
            dao.saveStockItem(stockitem);
            dao.commitTransaction();
        } else {
            StockItem item = dao.findStockItem(stockitem.getId());
            if (item.getName().toLowerCase().equals(stockitem.getName().toLowerCase())) {
                if (item.getPrice() == stockitem.getPrice()) {
                    item.setQuantity(item.getQuantity() + stockitem.getQuantity());
                    log.info("existing item quantity updated");
                } else {
                    String message = "Warehouse item price doesn't match new item price";
                    dao.rollbackTransaction();
                    log.debug(message);
                    throw new SalesSystemException(message);
                }
            } else {
                String message = "This barcode is already used for another product";
                dao.rollbackTransaction();
                log.debug(message);
                throw new SalesSystemException(message);
            }
        }

    }

    public SalesSystemDAO getDao() {
        return dao;
    }
}
