package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import static junit.framework.Assert.assertEquals;

public class addItemShoppingCart {

    @Test
    public void testAddingExistingItem() {  //valma
        SalesSystemDAO tester = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(tester);
        SoldItem item = new SoldItem(tester.findStockItem(1), 4);
        cart.addItem(item);
        int quantityBefore = tester.findStockItem(1).getQuantity();
        cart.submitCurrentPurchase();
        int quantityAfter = tester.findStockItem(1).getQuantity();
        assertEquals(quantityBefore, quantityAfter+4);
    }


    @Test
    public void testAddingNewItem() {  //valma
        SalesSystemDAO tester = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(tester);
        SoldItem item = new SoldItem(tester.findStockItem(1), 4);
        cart.addItem(item);
        List<SoldItem> cartitems = cart.getAll();
        List<SoldItem> wanted = new ArrayList<>();
        wanted.add(item);
        assertEquals(cartitems, wanted);
    }


    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithNegativeQuantity() {  // valma
        SalesSystemDAO tester = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(tester);
        cart.addItem(new SoldItem(tester.findStockItem(1), -4));
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithQuantityTooLarge() { //valmis
        SalesSystemDAO tester = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(tester);
        cart.addItem(new SoldItem(tester.findStockItem(1), 33));
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithQuantitySumTooLarge() { //valmis
        SalesSystemDAO tester = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(tester);
        cart.addItem(new SoldItem(tester.findStockItem(1), 3));
        cart.addItem(new SoldItem(tester.findStockItem(1), 3));
    }
}