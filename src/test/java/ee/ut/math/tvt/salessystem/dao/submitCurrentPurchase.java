package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldShoppingCart;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class submitCurrentPurchase {

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {  //valmis
        SalesSystemDAO tester = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(tester);
        cart.addItem(new SoldItem(tester.findStockItem(1), 3));
        int quantityBefore = tester.findStockItem(1).getQuantity();
        cart.submitCurrentPurchase();
        int quantityAfter = tester.findStockItem(1).getQuantity();
        assertEquals(quantityBefore, quantityAfter + 3);
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() { //valma
        SalesSystemDAO tester = mock(SalesSystemDAO.class);
        ShoppingCart cart = new ShoppingCart(tester);
        cart.submitCurrentPurchase();

        InOrder orderVerifier = Mockito.inOrder(tester);
        orderVerifier.verify(tester).beginTransaction();
        orderVerifier.verify(tester).commitTransaction();
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem() { //valmis
        SalesSystemDAO tester = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(tester);
        SoldItem soldItem = new SoldItem(tester.findStockItem(2), 3);
        SoldItem soldItem2 = new SoldItem(tester.findStockItem(1), 2);
        cart.addItem(soldItem);
        cart.addItem(soldItem2);
        cart.submitCurrentPurchase();
        SoldShoppingCart soldShoppingCart = tester.findSoldShoppingCart(1);
        assertEquals(soldShoppingCart, tester.findSoldShoppingCart(1));

        List<SoldItem> items = soldShoppingCart.getAll();
        List<SoldItem> wantedList = new ArrayList<>();
        wantedList.add(soldItem);
        wantedList.add(soldItem2);
        assertEquals(items, wantedList);
    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() {  //valmis
        SalesSystemDAO tester = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(tester);
        SoldItem soldItem = new SoldItem(tester.findStockItem(2), 3);
        cart.addItem(soldItem);
        cart.submitCurrentPurchase();
        Timestamp time = new Timestamp(System.currentTimeMillis());
        List<SoldShoppingCart> soldcarts = tester.getAllSoldShoppingCarts();
        SoldShoppingCart soldcart = soldcarts.get(0);

        long milliseconds = abs(time.getTime() - soldcart.getTimestamp().getTime());
        int secs = (int) milliseconds / 1000;
        int passed = 1;
        if (secs > 2) {
            passed = 0;
        }
        assertEquals(1, passed);
    }

    @Test
    public void testCancellingOrder() {  //valmis
        SalesSystemDAO tester = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(tester);
        cart.addItem(new SoldItem(tester.findStockItem(1), 3));
        cart.cancelCurrentPurchase();
        SoldItem soldItem = new SoldItem(tester.findStockItem(2), 3);
        cart.addItem(soldItem);
        cart.submitCurrentPurchase();
        List<SoldItem> items = tester.findSoldShoppingCart(1).getAll();
        assertEquals(soldItem, items.get(0));
    }

    @Test
    public void testCancellingOrderQuanititesUnchanged() {  //valmis
        SalesSystemDAO tester = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(tester);
        cart.addItem(new SoldItem(tester.findStockItem(1), 3));
        int quantityBefore = tester.findStockItem(1).getQuantity();
        cart.cancelCurrentPurchase();
        int quantityAfter = tester.findStockItem(1).getQuantity();
        assertEquals(quantityBefore, quantityAfter);
    }
}
