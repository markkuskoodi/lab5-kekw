package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Null;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class addItemWarehouse {

    @Test
    public void testAddingItemBeginsAndCommitsTransaction() { //valmis
        SalesSystemDAO tester = mock(SalesSystemDAO.class);
        Warehouse warehouse = new Warehouse(tester);
        StockItem item = new StockItem(0L, "testItem", 11.1, 22);
        warehouse.addProduct(item);

        InOrder orderVerifier = Mockito.inOrder(tester);
        orderVerifier.verify(tester).beginTransaction();
        orderVerifier.verify(tester).commitTransaction();
    }

    @Test
    public void testAddingNewItem() {  //valma
        SalesSystemDAO tester = mock(SalesSystemDAO.class);
        StockItem item = new StockItem(5L, "Onion", 2.0, 3);
        tester.saveStockItem(item);
        verify(tester, times(1)).saveStockItem(item);
    }

    @Test
    public void testAddingExistingItem() {
        SalesSystemDAO tester = mock(SalesSystemDAO.class);
        Warehouse warehouse = new Warehouse(tester);
        StockItem item = new StockItem(1L, "testItem", 11.1, 22);
        warehouse.addProduct(item);
        int quantityBefore = item.getQuantity();
        StockItem existingItem = new StockItem(1L, "testItem", 11.1, 22);
        warehouse.addProduct(existingItem);
        int quantityAfter = tester.findStockItem(1L).getQuantity();


        assertTrue(quantityBefore + 3 == quantityAfter);
        //verify(tester, never()).saveStockItem(item);
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithNegativeQuantity() {   //valmis
        SalesSystemDAO tester = new InMemorySalesSystemDAO();
        StockItem item = new StockItem(0L, "negative", 3.2, -2);
        tester.saveStockItem(item);
    }
}