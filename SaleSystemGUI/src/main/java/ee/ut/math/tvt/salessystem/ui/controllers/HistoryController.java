package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldShoppingCart;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.awt.event.MouseEvent;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {

    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;
    private final List<SoldShoppingCart> soldPurchases;
    private static final Logger log = LogManager.getLogger(HistoryController.class);

    @FXML
    private Button showLast10;
    @FXML
    private Button showAll;
    @FXML
    private Button showBetweenDates;
    @FXML
    private TableView<SoldShoppingCart> HistoryTableView;
    @FXML
    private TableView<SoldItem> CartTableView;
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;

    public HistoryController(SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
        this.soldPurchases = dao.getAllSoldShoppingCarts();


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TODO: implement
        HistoryTableView.setOnMouseClicked(event -> {
            if(event.getClickCount() == 1){
                CartTableView.setItems(FXCollections.observableList(HistoryTableView.getSelectionModel().getSelectedItem().getAll()));
                CartTableView.refresh();
            }
        });
    }

    public void showLastTenClicked(){
        log.debug("starting to show last ten purchases");
        if (soldPurchases.size() == 0) {
            error("There are no previous purchases to see");
        } else if (soldPurchases.size() <= 10) {
            HistoryTableView.setItems(FXCollections.observableList(soldPurchases));
            HistoryTableView.refresh();
            CartTableView.getItems().clear();//
            CartTableView.refresh();//

        } else {
            List<SoldShoppingCart> lastTen = new ArrayList<>();
            for (int i = soldPurchases.size() - 1, j = 0; j <= 10; i--, j++) {
                lastTen.add(soldPurchases.get(i));
            }
            HistoryTableView.setItems(FXCollections.observableList(lastTen));
            HistoryTableView.refresh();
            CartTableView.getItems().clear();//
            CartTableView.refresh();//

        }

        log.debug("showing last 10 purchases done");

    }

    public void showAllClicked(){
        log.debug("starting to show all purchases");
        HistoryTableView.setItems(FXCollections.observableList(soldPurchases));

        HistoryTableView.refresh();
        CartTableView.getItems().clear();//
        CartTableView.refresh();//

        log.debug("showing all purchases");
    }

    public void showBetweenDatesClicked(){
        log.debug("starting to show purchases between dates");
        List<SoldShoppingCart> betweenDates = new ArrayList<>();
        for(SoldShoppingCart cart : soldPurchases){
            if((startDate.getValue()).compareTo(cart.getDate()) <= 0 && (endDate.getValue()).compareTo(cart.getDate()) >= 0){
                betweenDates.add(cart);
            }
        }
        HistoryTableView.setItems(FXCollections.observableList(betweenDates));
        HistoryTableView.refresh();
        CartTableView.getItems().clear();//
        CartTableView.refresh();//
        log.debug("showing all purchases between selected dates");
    }

    private void error(String s) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Error");
        alert.setContentText(s);
        alert.showAndWait();
    }
}
