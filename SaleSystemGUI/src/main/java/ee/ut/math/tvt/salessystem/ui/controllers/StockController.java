package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class StockController implements Initializable {

    private final SalesSystemDAO dao;
    private final Warehouse warehouse;

    private static final Logger log = LogManager.getLogger(StockController.class);

    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItem;
    @FXML
    private TableView<StockItem> warehouseTableView;

    public StockController(SalesSystemDAO dao, Warehouse warehouse) {
        this.dao = dao;
        this.warehouse = warehouse;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();

        this.barCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem();
                }
            }
        });
    }

    public void addProduct() {
        try {
            if (quantityField.getText().equals("") || nameField.getText().equals("") || priceField.getText().equals("")) {
                String message = "At least one of the fields is empty";
                log.debug(message);
                throw new SalesSystemException(message);
            } else {
                String name = nameField.getText();
                int amount;
                Double price;
                long barcode;
                try {
                    amount = Integer.parseInt(quantityField.getText());
                    price = Double.parseDouble(priceField.getText());
                    try {
                        if (barCodeField.getLength() == 0) {
                            StockItem item = new StockItem(0L, name, price, amount);
                            warehouse.addProduct(item);
                        } else {
                            barcode = Long.parseLong(barCodeField.getText());
                            StockItem item = new StockItem(barcode, name, price, amount);
                            warehouse.addProduct(item);
                        }
                        log.info("New item added to the existing stock.");
                        refreshStockItems();
                    } catch (NumberFormatException e) {
                        String s = "Barcode has to be a number";
                        System.out.println(s);
                        error(s);
                    }


                } catch (SalesSystemException e) {
                    error(e.getMessage());

                } catch (NumberFormatException e) {
                    String s = "Invalid input";
                    System.out.println(s);
                    error(s);
                }
            }
        } catch (SalesSystemException e) {
            error(e.getMessage());
        }
    }


    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }

    private void refreshStockItems() {
        warehouseTableView.setItems(FXCollections.observableList(dao.findStockItems()));
        warehouseTableView.refresh();
        log.info("Warehouse refreshed");
    }

    private void fillInputsBySelectedStockItem() {
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem != null) {
            nameField.setText(stockItem.getName());
            priceField.setText(String.valueOf(stockItem.getPrice()));
        } else {
            resetProductField();
        }
    }

    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private void resetProductField() {
        barCodeField.setText("");
        quantityField.setText("1");
        nameField.setText("");
        priceField.setText("");
    }

    private void error(SalesSystemException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Error");
        alert.setContentText(e.getMessage());
        alert.showAndWait();
    }

    private void error(String s) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Error");
        alert.setContentText(s);
        alert.showAndWait();
    }

}
