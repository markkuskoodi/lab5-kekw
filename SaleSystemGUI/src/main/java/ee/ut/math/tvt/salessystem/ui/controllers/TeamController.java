package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class TeamController implements Initializable {

    @FXML
    private Label teamName;
    @FXML
    private Label teamContact;
    @FXML
    private GridPane gridpane;
    @FXML
    private ImageView teamLogo;

    InputStream inputStream;

    private static final Logger log = LogManager.getLogger(TeamController.class);


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            Properties prop = new Properties();
            String file = "application.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(file);
            if (inputStream != null) {
                prop.load(new InputStreamReader(inputStream, "UTF-8"));
            } else {
                throw new FileNotFoundException("Ei leidnud faili");
            }

            String team = prop.getProperty("teamName");
            String contact = prop.getProperty("contact");
            String member1 = prop.getProperty("member1");
            String member2 = prop.getProperty("member2");
            String member3 = prop.getProperty("member3");
            String url = prop.getProperty("logo");

            teamName.setText(team);
            teamContact.setText(contact);
            gridpane.add(new Label(member1), 1, 2);
            gridpane.add(new Label(member2), 1, 3);
            gridpane.add(new Label(member3), 1, 4);

            Image img = new Image(url);
            teamLogo.setImage(img);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
