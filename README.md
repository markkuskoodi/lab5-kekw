# Team KEKW:
1. Markkus Koodi
2. Enri Täär
3. Keit Järve

## Homework 1:
https://bitbucket.org/markkuskoodi/lab5-kekw/wiki/Homework%201 

## Homework 2:
https://bitbucket.org/markkuskoodi/lab5-kekw/wiki/Homework%202

## Homework 3:
https://bitbucket.org/markkuskoodi/lab5-kekw/src/homework-3/

## Homework 4:
https://bitbucket.org/markkuskoodi/lab5-kekw/wiki/Homework%204

https://bitbucket.org/markkuskoodi/lab5-kekw/commits/tag/homework-4

## Homework 5:
https://bitbucket.org/markkuskoodi/lab5-kekw/commits/tag/homework-5

## Homework 6:
https://bitbucket.org/markkuskoodi/lab5-kekw/wiki/Homework%206

https://bitbucket.org/markkuskoodi/lab5-kekw/commits/tag/homework-6

## Homework 7:
https://bitbucket.org/markkuskoodi/lab5-kekw/wiki/Homework%207

We encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)