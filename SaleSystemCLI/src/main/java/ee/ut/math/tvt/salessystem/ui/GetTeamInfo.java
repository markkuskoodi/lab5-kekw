package ee.ut.math.tvt.salessystem.ui;

import java.io.*;
import java.util.Properties;

public class GetTeamInfo {
    String name = "";
    String leader = "";
    String members = "";
    InputStream inputStream;

    public String getTeamInfo() throws IOException {
        try {
            Properties prop = new Properties();
            String file = "application.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(file);
            if (inputStream != null) {
                prop.load(new InputStreamReader(inputStream, "UTF-8"));
            } else {
                throw new FileNotFoundException("Ei leidnud faili");
            }

            String team = prop.getProperty("teamName");
            String contact = prop.getProperty("contact");
            String member1 = prop.getProperty("member1");
            String member2 = prop.getProperty("member2");
            String member3 = prop.getProperty("member3");
            name = "Team name: " + team;
            leader = "Team leader: " + contact;
            members = "Team members: " + member1 + ", " + member2 + ", " + member3;
            System.out.println(name);
            System.out.println(leader);
            System.out.println(members);
        } catch (Exception e) {
            System.out.println("Ei tööta");
        } finally {
            inputStream.close();
        }
        return name;
    }
}
