package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldShoppingCart;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;
    private final Warehouse warehouse;
    private final List<SoldShoppingCart> soldPurchases;


    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
        warehouse = new Warehouse(dao);
        this.soldPurchases = dao.getAllSoldShoppingCarts();
    }

    public static void main(String[] args) throws Exception {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }

    private void showStock() {
        List<StockItem> stockItems = dao.findStockItems();
        log.debug("showStock starting");
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
        log.debug("showStock done");
    }

    private void showCart() {
        log.debug("showCart starting");
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
        log.debug("showCart done");
    }

    private void showAllHistory() {
        log.debug("showAllHistory starting");
        System.out.println("\n" + "-".repeat(30));
        if (soldPurchases.size() == 0) {
            System.out.println("There are no previous purchases to see");
        } else {
            for (SoldShoppingCart cart : soldPurchases) {
                System.out.println("ID" + " ".repeat(4) + "Date" + " ".repeat(9) + "Time" + " ".repeat(7) + "Sum");
                System.out.println(cart.getId() + " ".repeat(2) + cart.getDate() + " ".repeat(3) + cart.getTime() + " ".repeat(2) + cart.getSum());
            }
        }
        System.out.println("-".repeat(30));
        log.debug("showAllHistory ending");
    }

    private void showSelectedShoppingCart(Long index) {
        log.debug("Starting to show selected shopping cart");
        System.out.println("-------------------------");
        if (dao.getSoldIdCount() < index) {
            System.out.println("Shoppingcart with selected id doesn't exist");
        } else {
            SoldShoppingCart soldCart = dao.findSoldShoppingCart(index);
            List<SoldItem> items = soldCart.getAll();
            for (SoldItem item : items) {
                System.out.println(item.getId() + " " + item.getName() + " " + item.getPrice() + " " + item.getQuantity() + " " + item.getQuantity() * item.getPrice());
            }
        }
        System.out.println("-------------------------");
    }

    private void showLastTenHistory() {
        log.debug("Starting to show selected shopping cart");
        System.out.println("-------------------------");
        String header = "ID" + " ".repeat(4) + "Date" + " ".repeat(9) + "Time" + " ".repeat(7) + "Sum";

        if (soldPurchases.size() == 0) {
            System.out.println("There are no previous purchases to see");
        } else if (soldPurchases.size() <= 10) {
            for (SoldShoppingCart cart : soldPurchases) {
                System.out.println(header);
                System.out.println(cart.getId() + " ".repeat(2) + cart.getDate() + " ".repeat(3) + cart.getTime() + " ".repeat(2) + cart.getSum());
            }
        } else {
            for (int i = 1; i <= 10; i++) {
                SoldShoppingCart cart = soldPurchases.get(soldPurchases.size() - i);
                System.out.println(header);
                System.out.println(cart.getId() + " ".repeat(2) + cart.getDate()  + "".repeat(3) + cart.getTime() + " ".repeat(2) + cart.getSum());
            }
        }

        System.out.println("-------------------------");
    }

    private void showBetweenDates(LocalDate startDate, LocalDate endDate) {
        log.debug("Starting showing purcheses between given dates.");
        System.out.println("-------------------------");
        for (SoldShoppingCart cart : soldPurchases) {
            if ((startDate).compareTo(cart.getDate()) <= 0 && (endDate).compareTo(cart.getDate()) >= 0) {
                System.out.println("\nID" + " ".repeat(4) + "Date" + " ".repeat(9) + "Time" + " ".repeat(7) + "Sum");
                System.out.println(cart.getId() + " ".repeat(2) + cart.getDate() + " ".repeat(3) + cart.getTime() + " ".repeat(2) + cart.getSum());
            }
        }
        System.out.println("-------------------------");
    }

    private void showTeam() throws IOException {
        log.debug("showTeam started");
        System.out.println("-------------------------");
        GetTeamInfo a = new GetTeamInfo();
        a.getTeamInfo();
        System.out.println("-------------------------");
        log.debug("showTeam done");
    }

    private void addItemWarehouse(String[] c) {
        try {
            if (c.length == 4) {
                String nameRaw = c[1];
                if (nameRaw.length() < 2) {
                    System.out.println("Invalid input");
                } else {
                    if (nameRaw.charAt(0) == '"' && nameRaw.charAt(nameRaw.length()-1) == '"') {
                        String name = nameRaw.substring(1, nameRaw.length() - 1);
                        double price = Double.parseDouble(c[2]);
                        int amount = Integer.parseInt(c[3]);
                        StockItem item = new StockItem(0L, name, price, amount);
                        warehouse.addProduct(item);
                    } else {
                        System.out.println("Invalid input");
                    }
                }
            } else {
                String nameRaw = c[1];
                String name = nameRaw.substring(1, nameRaw.length() - 1);
                double price = Double.parseDouble(c[2]);
                int amount = Integer.parseInt(c[3]);
                long barcode = Long.parseLong(c[4]);
                StockItem item = new StockItem(barcode, name, price, amount);
                warehouse.addProduct(item);
            }
        } catch (NumberFormatException e) {
            System.out.println("Invalid input");
        } catch (SalesSystemException e) {
            System.out.println("This barcode is already used by different item");
        }
    }

    private void addItemCart(String[] c) {
        try {
            long idx = Long.parseLong(c[1]);
            int amount = Integer.parseInt(c[2]);
            StockItem item = dao.findStockItem(idx);
            if (item != null) {
                cart.addItem(new SoldItem(item, Math.min(amount, item.getQuantity())));
            } else {
                System.out.println("no stock item with id " + idx);
            }
        } catch (NumberFormatException e) {
            System.out.println("Invalid input");
        } catch (SalesSystemException | NoSuchElementException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void printUsage() {
        System.out.println("-------------------------\n");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("\nPOINT-OF-SALE");
        System.out.println("c\t\tShow cart contents");
        System.out.println("a IDX NR \tAdd NR of stock item with index IDX to the cart");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("\nWAREHOUSE");
        System.out.println("i I PR NR (IDX) \tAdd product I (must be inside quotation marks) with price PR, amount NR and index IDX to the warehouse");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("\nHISTORY");
        System.out.println("ha\t\t Shows all previous purchases (id, purchase time, cart sum)");
        System.out.println("ha10\t\t Shows last 10 purchases");
        System.out.println("ha S E\t\t Shows all purchases between date S(yyyy-mm-dd) and date E(yyyy-mm-dd)");
        System.out.println("ha IDX\t\t Shows shopping cart of selected purchase with index IDX");
        System.out.println("\nTEAM");
        System.out.println("t\t\tShow team view");
        System.out.println("\n-------------------------");
    }

    private void processCommand(String command) throws IOException {
        String[] c = command.split(" (?=([^\"]*\"[^\"]*\")*[^\"]*$)");

        if (c[0].equals("h"))
            printUsage();
        else if (c[0].equals("q")) {
            log.debug("System exit");
            System.exit(0);
        } else if (c[0].equals("w")) {
            showStock();
        } else if (c[0].equals("c"))
            showCart();
        else if (c[0].equals("p")) {
            cart.submitCurrentPurchase();
            log.info("Purchase done");
        } else if (c[0].equals("r")) {
            cart.cancelCurrentPurchase();
            log.info("Current purchase cancelled");
        } else if (c[0].equals("i")) {
            addItemWarehouse(c);
        } else if (c[0].equals("a") && c.length == 3) {
            addItemCart(c);
        } else if (c[0].equals("t")) {
            showTeam();
        } else if (c[0].equals("ha") && c.length == 1) {
            showAllHistory();
        } else if (c[0].equals("ha") && c.length == 2) {
            try {
                long index = Long.parseLong(c[1]);
                showSelectedShoppingCart(index);
            } catch (NumberFormatException e) {
                System.out.println("Invalid input");
            } catch (SalesSystemException | NoSuchElementException e) {
                log.error(e.getMessage(), e);
            }
        } else if (c[0].equals("ha10")) {
            showLastTenHistory();
        } else if (c[0].equals("ha") && c.length == 3) {
            String start = c[1];
            String end = c[2];
            try {
                LocalDate s = LocalDate.parse(start);
                LocalDate e = LocalDate.parse(end);
                showBetweenDates(s, e);
            } catch (DateTimeException e) {
                System.out.println("Invalid input");
            }
        } else {
            System.out.println("unknown command");
        }
    }

}
